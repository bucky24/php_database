<?php

namespace bucky24\PHPDatabase;

class Model {
    private static $PREFIX = "";
    private static $database = "";
    private $table = "";
    private $fields = array();
    private $indexes = array();
    private $version = 0;

    function __construct($table, $fields, $version) {
        $this->table = $table;
        $this->fields = $fields;
        $this->version = $version;

        if (!array_key_exists("id", $this->fields)) {
            $this->fields['id'] = array(
                "type" => "integer",
                "autoincrement" => true,
            );
        }
    }

    static function setTablePrefix($newPrefix) {
        Model::$PREFIX = $newPrefix;
    }

    static function connect($host, $user, $password, $db) {
        global $dbConn;
        Model::$database = $db;
        $dbConn = new \mysqli($host, $user, $password, $db);
    }

    function query($query) {
        global $dbConn;
        $result = $dbConn->query($query);

        if ($result === false) {
            error_log("Error executing $query: " . $dbConn->error . "\n");
        }

        return $result;
    }

    function getTableName() {
        return Model::getTable($this->table);
    }

    static function getTable($table) {
        return Model::$PREFIX . $table;
    }

    function init() {
        global $dbConn, $PREFIX;

        $valid_types = array(
            "varchar",
            "text",
            "int",
            "integer",
            "boolean",
            "tinyint",
        );

        $versionsTable = $PREFIX . "table_versions";

        // first make sure we can find our version table
        $query = "SELECT * 
        FROM information_schema.tables
        WHERE table_schema = '" . Model::$database . "' 
            AND table_name = '$versionsTable'
        LIMIT 1;";

        $result = $this->query($query);
        $count = $result->num_rows;

        if ($count === 0) {
            error_log("Unable to find version table, creating...\n");

            $query = "CREATE TABLE $versionsTable(name VARCHAR(255), version INT)";
            $this->query($query);
        }

        $VERSION_CHECK_STMT = $dbConn->prepare("SELECT version FROM $versionsTable WHERE name = ?");
        $VERSION_CHECK_NAME = '';
        $VERSION_CHECK_STMT->bind_param('s', $VERSION_CHECK_NAME);

        $VERSION_INSERT_STMT = $dbConn->prepare("INSERT INTO $versionsTable(name, version) VALUES(?, ?)");
        $VERSION_INSERT_NAME = '';
        $VERSION_INSERT_VERSION = 0;
        $VERSION_INSERT_STMT->bind_param('si', $VERSION_INSERT_NAME, $VERSION_INSERT_VERSION);

        // now check the version of our table

        $VERSION_CHECK_NAME = $this->getTableName();
        $VERSION_CHECK_STMT->execute();
        $result = $VERSION_CHECK_STMT->get_result();
        $results = $result->fetch_all(MYSQLI_ASSOC);

        $found_version = null;
        if (count($results) > 0) {
            $found_version = $results[0]['version'];
        }

        if ($found_version === null) {
            error_log("Table " . $this->getTableName() . " not found, creating\n");
            
            // hard to parameterize here because we are doing a table create
            $query = "CREATE TABLE " . $this->getTableName() . "(";

            $fieldList = array();
            $key = array();
            $fieldNames = array();
            foreach ($this->fields as $name => $field) {
                $type = $field['type'];
                if (array_search($type, $valid_types) === false) {
                    throw new \Error("Invalid type for field '$name': $type");
                }
                if ($type === "varchar") {
                    $type = "varchar(255)";
                }
                $fieldQuery = $name . " " . $type;
                if (isset($field['autoincrement']) && $field['autoincrement']) {
                    $fieldQuery .= " AUTO_INCREMENT";
                    $key[] = "`" . $name . "`";
                }
                $fieldNames[] = $name;
                $fieldList[] = $fieldQuery;
            }
            if (count($key) > 0) {
                $fieldList[] = "PRIMARY KEY (" . implode(", ", $key) . ")";
            }
            $query .= implode(", ", $fieldList) . ")";

            $result = $this->query($query);

            if ($result === false) {
                throw new \Error("Can't create table " . $this->getTableName() . ": " . $dbConn->error);
            }

            // now populate the indexes, if any
            foreach ($this->indexes as $index) {
                $name = $index['name'];
                $fieldList = array();
                foreach ($index['fields'] as $field) {
                    if (!in_array($field, $fieldNames)) {
                        throw new \Error("Table " . $this->getTableName() . " can't have field '$field' in index, it is not defined as a table field");
                    }
                    $fieldList[] = "`" . $field . "`";
                }
                $query = "CREATE INDEX `" . $this->getTableName() . "_" . $name . "` ON " . $this->getTableName() . " (" . implode(", ", $fieldList) . ")";
                $result = $this->query($query);
                if ($result === false) {
                    throw new \Error("`Can't create index on " . $this->getTableName());
                }
            }

            $VERSION_INSERT_NAME = $this->getTableName();
            $VERSION_INSERT_VERSION = $this->version;
            $VERSION_INSERT_STMT->execute();
            $found_version = $this->version;

            error_log("Table created!");
        }

        if ($found_version !== $this->version) {
            error_log("Table version for table " . $this->getTableName() . " does not match! Code has " . $this->version . ", db has $found_version");

            // look up all fields on the table
            $query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" . Model::$database . "' AND TABLE_NAME = '" . $this->getTableName() . "';";
            $result = $this->query($query);
            $columns = mysqli_fetch_all($result, MYSQLI_ASSOC);

            // compare against fields in the system
            $model_fields = array_keys($this->fields);
        
            foreach ($columns as $column) {
                $column = $column['COLUMN_NAME'];
                $index = array_search($column, $model_fields);
                if ($index !== false) {
                    unset($model_fields[$index]);
                }
            }

            // add in the missing columns
            foreach ($model_fields as $field) {
                $data = $this->fields[$field];
                error_log("Field \"$field\" missing in db, creating it now");

                $type = $data['type'];
                if (array_search($type, $valid_types) === false) {
                    throw new \Error("Invalid type for field '$field': $type");
                }
                if ($type === "varchar") {
                    $type = "varchar(255)";
                }

                $query = "ALTER TABLE " . $this->getTableName() . " ADD COLUMN $field $type";

                if (isset($data['autoincrement']) && $data['autoincrement']) {
                    $fieldQuery .= " AUTO_INCREMENT, ADD PRIMARY KEY($field)";
                }

                $result = $this->query($query);
                if (!$result) {
                    throw new \Error("Can't create new column $field: " . $dbConn->error);
                }
            }

            // update the version
            $VERSION_UPDATE_STMT = $dbConn->prepare("UPDATE $versionsTable SET version = ? WHERE name = ?");
            $VERSION_UPDATE_NAME = $this->getTableName();
            $VERSION_UPDATE_VERSION = $this->version;
            $VERSION_UPDATE_STMT->bind_param('is', $VERSION_UPDATE_VERSION, $VERSION_UPDATE_NAME);
            $result = $VERSION_UPDATE_STMT->execute();
            if (!$result) {
                throw new \Error("Could not migrate!" . $dbConn->error);
            }
        }
    }

    function insert($data = array()) {
        global $dbConn;

        $query = "INSERT INTO " . $this->getTableName() . "(";

        $types = "";
        $bind_params = array();
        $fieldNames = array();
        $questionMarks = array();
        foreach ($data as $key=>$value) {
            if (!array_key_exists($key, $this->fields)) {
                throw new \Error("Cannot insert data into " . $this->getTableName() . ": field $key does not exist");
            }

            $field = $this->fields[$key];

            $dataType = "";
            if ($field['type'] === "int" || $field['type'] === 'tinyint' || $field['type'] === "integer") {
                $dataType = "i";
            } else if ($field['type'] === "varchar" || $field['type'] === "text") {
                $dataType = 's';
            } else if ($field['type'] === "boolean") {
                $dataType = 'i';
            }
            $types .= $dataType;

            // mysqli is so strange honestly this should not be this hard
            $bind_name = 'bind' . $key;
            $$bind_name = $value;
            $bind_params[] = &$$bind_name;
            $fieldNames[] = $key;
            $questionMarks[] = "?";
        }

        $query .= implode(", ", $fieldNames) . ") VALUES(" . implode(", ", $questionMarks) . ")";

        $stmt = $dbConn->prepare($query);

        array_unshift($bind_params, $types);

        if (count($bind_params) > 1) {
            $return = call_user_func_array(array($stmt,'bind_param'), $bind_params);
        }

        $result = $stmt->execute();
        if ($result === false) {
            throw new \Error("Error inserting into " . $this->getTableName() . ": " . $dbConn->error);
        }

        $id = $dbConn->insert_id;

        return $id;
    }

    function get($id) {
        $rows = $this->search(array(
            "id" => $id,
        ), "id", 1);

        if (count($rows) === 0) {
            return null;
        }

        return $rows[0];
    }

    function search($data = array(), $order = null, $limit = null, $offset = null) {
        global $dbConn;

        $query = "SELECT * FROM " . $this->getTableName() . " WHERE ";

        $types = "";
        $bind_params = array();
        $whereData = array();
        foreach ($data as $key=>$value) {
            if (!array_key_exists($key, $this->fields)) {
                throw new \Error("Cannot select data from table " . $this->getTableName() . ": field $key does not exist");
            }

            $field = $this->fields[$key];

            $dataType = "";
            if ($field['type'] === "int" || $field['type'] === 'tinyint' || $field['type'] === "integer") {
                $dataType = "i";
            } else if ($field['type'] === "varchar" || $field['type'] === "text") {
                $dataType = 's';
            } else if ($field['type'] === "boolean") {
                $dataType = 'i';
                $value = $value === "true";
            }

            // mysqli is so strange honestly this should not be this hard
            if (is_array($value)) {
                $whereLine =  "$key IN (";
                $questionMarks = array();
                foreach ($value as $index=>$valueEntry) {
                    $bind_name = 'bind' . $key . $index;
                    $$bind_name = $valueEntry;
                    $bind_params[] = &$$bind_name;
                    $types .= $dataType;
                    $questionMarks[] = "?";
                }
                $whereLine .= implode(", ", $questionMarks) . ")";
                $whereData[] = $whereLine;
            } else {
                $bind_name = 'bind' . $key;
                $$bind_name = $value;
                $bind_params[] = &$$bind_name;
                $types .= $dataType;
                $whereData[] = "$key = ?";
            }
        }
        
        if (count($data) === 0) {
            $whereData[] = "1";
        }

        $query .= implode(" AND ", $whereData);

        if ($order !== null) {
            $query .= " ORDER BY $order";
        }

        if ($limit !== null) {
            $query .= " LIMIT ?";
            $bind_name = 'bindLimit';
            $$bind_name = $limit;
            $bind_params[] = &$$bind_name;
            $types .= "i";
            // offset only valid if there's a limit
            if ($offset !== null) {
                $query .= " OFFSET ?";
                $bind_name = 'bindOffset';
                $$bind_name = $offset;
                $bind_params[] = &$$bind_name;
                $types .= "i";
            }
        }

        $stmt = $dbConn->prepare($query);

        array_unshift($bind_params, $types);

        if (count($bind_params) > 1) {
            $return = call_user_func_array(array($stmt,'bind_param'), $bind_params);
        }

        $result = $stmt->execute();
        if ($result === false) {
            throw new \Error("Unable to select on table " . $this->getTableName() . ": " . $dbConn->error);
        }
        $result = $stmt->get_result();
        $results = $result->fetch_all(MYSQLI_ASSOC);

        return $results;
    }

    function update($id, $changeList) {
        global $dbConn;

        $searchList = array(
            "id" => $id,
        );

        $query = "UPDATE " . $this->getTableName() . " SET ";

        $types = "";
        $bind_params = array();
        $setData = array();
        foreach ($changeList as $key=>$value) {
            if (!array_key_exists($key, $this->fields)) {
                throw new \Error("Cannot update data from table " . $this->getTableName() . ": field $key does not exist");
            }

            $field = $this->fields[$key];

            $dataType = "";
            if ($field['type'] === "int" || $field['type'] === 'tinyint' || $field['type'] === "integer") {
                $dataType = "i";
            } else if ($field['type'] === "varchar" || $field['type'] === "text") {
                $dataType = 's';
            } else if ($field['type'] === "boolean") {
                $dataType = 'i';
                $value = $value === "true";
            }

            if ($dataType === "") {
                error_log("Warning: unknown data type for field $key: " . $field['type']);
            }

            // mysqli is so strange honestly this should not be this hard
            $bind_name = 'change_bind' . $key;
            $$bind_name = $value;
            $bind_params[] = &$$bind_name;
            $types .= $dataType;
            $setData[] = "$key = ?";
        }

        $query .= implode(", ", $setData). " WHERE id = ?";
        $types .= "i";
        $bind_params[] = &$id;

        $stmt = $dbConn->prepare($query);

        array_unshift($bind_params, $types);

        if (count($bind_params) > 1) {
            $return = call_user_func_array(array($stmt,'bind_param'), $bind_params);
        }

        $result = $stmt->execute();
        if ($result === false) {
            throw new \Error("Unable to update on table " . $this->getTableName() . ": " . $dbConn->error);
        }

        return true;
    }

    function delete($id) {
        global $dbConn;

        $query = "DELETE FROM " . $this->getTableName() . " WHERE id = ?";

        $stmt = $dbConn->prepare($query);
        $stmt->bind_param("i", $id);

        $result = $stmt->execute();
        if ($result === false) {
            throw new \Error("Unable to delete on table " . $this->getTableName() . ": " . $dbConn->error);
        }

        return true;
    }

    function count() {
        $query = "SELECT COUNT(*) as `count` FROM " . $this->getTableName();
        $result = $this->query($query);

        $result = $result->fetch_assoc();
        return $result['count'];
    }

    static function raw($query, $params = array()) {
        global $dbConn;

        $types = ""; 
        $bind_params = array();
        foreach ($params as $index => $value) {
            $types .= "s";

            $bind_name = 'bind' . $index;
            $$bind_name = $value;
            $bind_params[] = &$$bind_name;
        }

        $stmt = $dbConn->prepare($query);
        if (!$stmt) {
            error_log("Could not create statement: " . $dbConn->error);
        }
        array_unshift($bind_params, $types);

        if (count($bind_params) > 1) {
            $return = call_user_func_array(array($stmt,'bind_param'), $bind_params);
        }

        $result = $stmt->execute();
        if ($result === false) {
            throw new \Error("Unable to run raw query $query: " . $stmt->error);
        }
        $result = $stmt->get_result();

        if (substr($query, 0, 4) === "DROP" || substr($query, 0, 6) === "DELETE") {
            return $result !== false;
        }

        $results = $result->fetch_all(MYSQLI_ASSOC);

        return $results;
    }
}

?>