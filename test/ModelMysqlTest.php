<?php

use PHPUnit\Framework\TestCase;
use bucky24\PHPDatabase\Model;

function getTableNames() {
    $tables = Model::raw("SELECT table_name as name FROM information_schema.tables WHERE table_type='BASE TABLE' AND table_schema = 'test_db'");

    $table_names = array();
    foreach ($tables as $table) {
        $table_names[] = $table['name'];
    }

    return $table_names;
}

final class ModelMysqlTest extends TestCase {
    public static function setupBeforeClass(): void {
        Model::connect("localhost", "root", "", "test_db");
    }

    public function tearDown(): void {
        $tables = Model::raw("SELECT table_name as name FROM information_schema.tables WHERE table_type='BASE TABLE' AND table_schema = 'test_db'");

        foreach ($tables as $table) {
            Model::raw("DROP TABLE " . $table['name']);
        }
    }

    public function testVersionsTableCreated(): void {
        $model = new Model("test_table", array(), 1);
        $model->init();

        $table_names = getTableNames();

        $this->assertTrue(in_array("table_versions", $table_names));
    }

    public function testTableCreated(): void {
        $model = new Model("test_table", array(
            "name" => array("type" => "varchar"),
        ), 1);
        $model->init();

        $table_names = getTableNames();

        $this->assertTrue(in_array("test_table", $table_names));

        $fields = Model::raw("DESCRIBE test_table;");

        $this->assertEquals(count($fields), 2);
        $this->assertEquals($fields[0], array(
            "Field" => "name",
            "Type" => "varchar(255)",
            "Null" => "YES",
            "Key" => null,
            "Default" => null,
            "Extra" => null,
        ));
        $this->assertEquals($fields[1], array(
            "Field" => "id",
            "Type" => "int",
            "Null" => "NO",
            "Key" => "PRI",
            "Default" => null,
            "Extra" => "auto_increment",
        ));

        $version = Model::raw("SELECT version FROM table_versions WHERE name = 'test_table'");
        $this->assertEquals($version[0]['version'], 1);
    }

    public function testInsertDataFailureBadField(): void {
        $model = new Model("test_table", array(
            "name" => array("type" => "varchar"),
        ), 1);
        $model->init();

        $message = null;
        try {
            $model->insert(array(
                "name" => "foo",
                "other_field" => "not inserted",
            ));
        } catch (Error $error) {
            $message = $error->getMessage();
        }

        $this->assertEquals($message, "Cannot insert data into test_table: field other_field does not exist");
    }

    public function testInsertData(): void {
        $model = new Model("test_table", array(
            "name" => array("type" => "varchar"),
        ), 1);
        $model->init();

        $model->insert(array(
            "name" => "foo",
        ));

        $rows = Model::raw("SELECT * FROM test_table");
        $this->assertEquals(count($rows), 1);
        $this->assertEquals($rows[0], array(
            "id" => 1,
            "name" => "foo",
        ));
    }

    public function testGetDataWithBadRow(): void {
        $model = new Model("test_table", array(
            "name" => array("type" => "varchar"),
        ), 1);
        $model->init();

        $row = $model->get(1);

        $this->assertNull($row);
    }

    public function testGetData(): void {
        $model = new Model("test_table", array(
            "name" => array("type" => "varchar"),
        ), 1);
        $model->init();

        $id = $model->insert(array(
            "name" => "foo",
        ));

        $row = $model->get($id);

        $this->assertEquals($row, array(
            "id" => 1,
            "name" => "foo",
        ));
    }

    public function testUpdateData(): void {
        $model = new Model("test_table", array(
            "name" => array("type" => "varchar"),
        ), 1);
        $model->init();

        $id = $model->insert(array(
            "name" => "foo",
        ));

        $model->update($id, array(
            "name" => "bob",
        ));
        
        $row = $model->get($id);

        $this->assertEquals($row, array(
            "id" => 1,
            "name" => "bob",
        ));
    }

    public function testSearch(): void {
        $model = new Model("test_table", array(
            "name" => array("type" => "varchar"),
        ), 1);
        $model->init();

        $id = $model->insert(array(
            "name" => "foo",
        ));
        $id2 = $model->insert(array(
            "name" => "foo",
        ));
        $id3 = $model->insert(array(
            "name" => "foo",
        ));
        $id4 = $model->insert(array(
            "name" => "foo",
        ));

        $rows = $model->search(array(
            "name" => "foo",
        ));

        $this->assertEquals(count($rows), 4);
        $this->assertEquals($rows[0], array(
            "id" => $id,
            "name" => "foo",
        ));
        $this->assertEquals($rows[1], array(
            "id" => $id2,
            "name" => "foo",
        ));
        $this->assertEquals($rows[2], array(
            "id" => $id3,
            "name" => "foo",
        ));
        $this->assertEquals($rows[3], array(
            "id" => $id4,
            "name" => "foo",
        ));
    }

    function testDelete(): void {
        $model = new Model("test_table", array(
            "name" => array("type" => "varchar"),
        ), 1);
        $model->init();

        $id = $model->insert(array(
            "name" => "foo",
        ));

        $model->delete($id);

        $row = $model->get($id);

        $this->assertEquals($row, null);
    }

    function testAddNewField(): void {
        $model = new Model("test_table", array(
            "name" => array("type" => "varchar"),
        ), 1);
        $model->init();

        $model = new Model("test_table", array(
            "name" => array("type" => "varchar"),
            "foo" => array("type" => "integer"),
        ), 2);
        $model->init();

        $id = $model->insert(array(
            "name" => "foo",
            "foo" => 5,
        ));

        $row = $model->get($id);

        $this->assertEquals($row, array(
            "id" => 1,
            "name" => "foo",
            "foo" => 5,
        ));
    }

    function testBoolean(): void {
        $model = new Model("test_table", array(
            "foo" => array("type" => "boolean"),
        ), 1);
        $model->init();

        $id = $model->insert(array(
            "foo" => true,
        ));

        $row = $model->get($id);
        $this->assertEquals($row, array(
            "id" => 1,
            "foo" => true,
        ));
    }

    function testInteger(): void {
        $model = new Model("test_table", array(
            "foo" => array("type" => "integer"),
        ), 1);
        $model->init();

        $id = $model->insert(array(
            "foo" => 3,
        ));

        $row = $model->get($id);
        $this->assertEquals($row, array(
            "id" => 1,
            "foo" => 3,
        ));
    }

    function testCreateTableBadType(): void {
        $model = new Model("test_table", array(
            "foo" => array("type" => "bool"),
        ), 1);

        $message = null;
        try {
            $model->init();
        } catch (Error $error) {
            $message = $error->getMessage();
        }

        $this->assertEquals($message, "Invalid type for field 'foo': bool");
    }
}

?>