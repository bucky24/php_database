# php_database

To install:

    composer config repositories.php_database git https://gitlab.com/bucky24/php_database.git
    composer require bucky24/php_database:1.1.1 (or latest version)

Example:

```
<?php

require __DIR__ . '/vendor/autoload.php';

use bucky24\PHPDatabase\Model as Model;

Model::connect("localhost", "root", "", "test_db");

$fooTable = new Model(
    "foo", 
    array(
        "foo" => array(
            "type" => "varchar",
        ),
    ),
    1,
);

$fooTable->init();

$id = $fooTable->insert(array(
    "field" => "value",
));

$items = $fooTable->get($id);

$items = $fooTable->search(array(
    "field" => "value",
));

$fooTable->update($id, array(
    "field" => "value",
));

$fooTable->delete($id);

?>
```